const assert = require('assert');
let fs = require('fs');

 // map that keeps track of all the items a user has bought

 //map that keeps track of all the items in listing
let itemsBought = {};


let itemsSold = {};


let allItems = {};


/*
Use this function to generate a new UID every time a user creates an account.
*/
function genUID() {
    return Math.floor(Math.random() * 1000);
}

function putItems(userID, value) {
    itemsBought[userID] = value;
    itemsSold[userID] = value;
}


function getItemsBought(userID) {
    var ret = itemsBought[userID];
    if(ret == undefined) {
        return null;
    }
    return ret;
}

function getItemsSold(userID) {
    
    var ret = itemsSold[userID];
    if(ret == undefined) {
        return null;
    }
    return ret;
}


/*
initializeUserIfNeeded adds the UID to our database unless it's already there
parameter: [uid] the UID of the user.
returns: undefined
*/
function initializeUserIfNeeded(uid) {
    var itemsBought = getItemsBought(uid);
    var itemsSold = getItemsSold(uid);
    if(itemsBought == null && itemsSold == null) {
        putItems(uid, []);
    }
}

/*
allItemsBought returns the IDs of all the items bought by a buyer
    parameter: [buyerID] The ID of the buyer
    returns: an array of listing IDs
*/
function allItemsBought(userID) {
    let items = itemsBought[userID];
    return items;
}

function allItemsSold(userID) {
    let items = itemsSold[userID];
    return items;
}

/* 
createListing adds a new listing to our global state.
This function is incomplete. You need to complete it.
    parameters: 
      [sellerID] The ID of the seller
      [price] The price of the item
      [blurb] A blurb describing the item
    returns: The ID of the new listing
*/
function createListing(sellerID, name, price, blurb, category) {
    let itemID = genUID();
    //console.log("ur in");

    let item = {
        sellerID: sellerID,
        name: name,
        price: price,
        description: blurb,
        cat: category
    }

    allItems[itemID] = item;
    fs.writeFileSync('listings.txt', JSON.stringify(allItems));

    if(allItems){ 
        return "successful" ;
    }
    else{
        return "failed to create listing";
    }

    //return ("listing #" + itemID + " created\nObject Values: " + JSON.stringify(allItems));

}

/* 
getItemDescription returns the description of a listing
    parameter: [listingID] The ID of the listing
    returns: An object containing the price and blurb properties.
*/
function getItemDescription(listingID) {
    let itemDesc = {
        name: allItems[listingID].name,
        price: allItems[listingID].price,
        description: allItems[listingID].description
    }
    return itemDesc;
}

/* 
buy changes the global state.
Another buyer will not be able to purchase that listing
The listing will no longer appear in search results
The buyer will see the listing in his history of purchases
The seller will see the listing in his history of items sold
    parameters: 
     [buyerID] The ID of buyer
     [sellerID] The ID of seller
     [listingID] The ID of listing
    returns: undefined
*/
function buy(buyerID, listingID) {
    let itemID = listingID;
    let item = allItems[itemID];
    itemsBought[buyerID] = itemsBought[buyerID].concat(item);    
    itemsSold[item.sellerID] = itemsSold[item.sellerID].concat(item); 

    fs.writeFileSync('itemsBought.txt', JSON.stringify(itemsBought));
    fs.writeFileSync('itemsSold.txt', JSON.stringify(itemsSold));  

    delete allItems[itemID];
}

/*
allListings returns the IDs of all the listings currently on the market
Once an item is sold, it will not be returned by allListings
    returns: an array of listing IDs
*/
function allListings() {
    for(var key in allItems){
        console.log(key);
        console.log(allItems[key]);
    }
    
}

/*
searchForListings returns the IDs of all the listings currently on the market
Once an item is sold, it will not be returned by searchForListings
    parameter: [searchTerm] The search string matching listing descriptions
    returns: an array of listing IDs
*/
function searchForListings(searchTerm) {
    let searchedItems = [];

    for(var key in allItems){
        let itemID = key;
        let catOfItem = allItems[key].cat;
        if (catOfItem === searchTerm){
            let item = {
                itemID: itemID,
                name: allItems[key].name,
                category: allItems[key].category,
                description: allItems[key].description, 
                price: allItems[key].price
            }
            searchedItems.push(item);
        }
        
    }
  
    return searchedItems;  

}

module.exports = {
    genUID, // This is just a shorthand. It's the same as genUID: genUID. 
    initializeUserIfNeeded,
    putItems,
    getItemsBought,
    createListing,
    getItemDescription,
    buy,
    allItemsSold,
    searchForListings, 
    allListings, 
    allItemsBought

    // Add all the other functions that need to be exported
}