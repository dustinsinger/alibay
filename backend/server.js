const express = require('express');
const app = express();

let fs = require('fs');

const alibay = require('./alibay');

let bodyParser = require('body-parser');
app.use(bodyParser.raw({ type: "*/*" }));

// let nodemailer = require('nodemailer');
// let kickbox = require('kickbox').client('live_fe4c15fed10d19a1605b6c704f9a7efeded9b145200556450719d8aba4a1959e').kickbox();

let users = {};
let storedUsers = fs.readFileSync('users.txt').toString();
users = JSON.parse(storedUsers);

let storedBought = fs.readFileSync('itemsBought.txt').toString();
alibay.itemsBought = JSON.parse(storedBought);

let storedSold = fs.readFileSync('itemsSold.txt').toString();
alibay.itemsSold = JSON.parse(storedSold);

let listingsStored = fs.readFileSync('listings.txt').toString();
alibay.allItems = JSON.parse(listingsStored);

// var transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//       user: 'theRealAlibay@gmail.com',
//       pass: 'jsissofun2018'
//     }
//   });

//   var mailOptions = {
//     from: 'theRealAlibay@gmail.com',
//     to: 'r.soumounoud@icloud.com',
//     subject: 'Sending Email using Node.js',
//     html: 'That was easy!'
//   };

//   transporter.sendMail(mailOptions, function(error, info){
//     if (error) {
//       console.log('error' + error);
//     } else {
//       console.log('Email sent: ' + info.response);
//     }
//   });

// var smtpTransport = nodemailer.createTransport('SMTP', {
//   service: 'Gmail',
//   auth: {
//     user: 'theRealAlibay@gmail.com',
//     pass: ''
//   }
// });

// var mailOptions = {
//     from: 'theRealAlibay@gmail.com',
//     to: 'r.soumounou@icloud.com',
//     subject: 'Hello world!',
//     text: 'Plaintext message example.'
// };

// smtpTransport.sendMail(mailOptions, function(error, info){
//     if (error) {
//       console.log('error' + error);
//     } else {
//       console.log('Email sent: ' + info.response);
//     }
//     smtpTransport.close();
//   });



function genUniqueID(){

    return '' + Math.floor(Math.random() * 1000000);

}

app.post('/login', (req, res) => {
    let parsedBody = JSON.parse(req.body.toString());
    let username = parsedBody.username;
    let password = parsedBody.password;
    let sessionID = genUniqueID();
    let loginSuccess = false;
    let resp = {};

    for(var key in users){
        let usrs = users[key].username;
        let pwds = users[key].password;
        if (usrs === username && pwds === password){
            resp.sessionID = sessionID;
            resp.userID = key;
            loginSuccess = true;
        }
    }

    if(loginSuccess){
        res.send(JSON.stringify(resp));
        
    }
    else{
        res.send(JSON.stringify({success: false}));
    }
   
    

});

app.post('/signUp', function (req, res) {
    let body = JSON.parse(req.body.toString());
    let userExist = false;
    
    for(var key in users){
        let usrs = users[key].username;
        if(usrs === body.username){
            res.send("username already exists");
            userExist = true;
        }
    }

    if(!userExist){

        let newUser = {
            username: body.username,
            password: body.password,
            firstName: body.firstName,
            lastName: body.lastName
        }
        
        let userID = genUniqueID();
        users[userID] = newUser;
        fs.writeFileSync('users.txt', JSON.stringify(users));
        alibay.initializeUserIfNeeded(userID);
        fs.writeFileSync('itemsBought.txt', JSON.stringify(alibay.itemsBought));
        fs.writeFileSync('itemsSold.txt', JSON.stringify(alibay.itemsSold));
        res.send(JSON.stringify({message: "success"}));

    }

    // kickbox.verify(emailToCheck, function (err, response) {
    //     if(response.body.reason === "accepted_email"){
    //         res.send("email exists");
    //     }
    //     else{
    //         res.send("email does not exist");
    //     }
    // });
});

app.post('/addItem', (req, res) => {
    let parsedBody = JSON.parse(req.body.toString());

    let userID =  parsedBody.userID;
    let price = parseInt(parsedBody.price);
    let name = parsedBody.name;
    let description = parsedBody.description;
    let category = parsedBody.category;

    let resp = alibay.createListing(userID, name, price, description, category); 
    res.send(JSON.stringify(resp));

});

app.post('/itemsSold', (req, res) =>{
    let parsedBody = JSON.parse(req.body.toString());
    let userID = parsedBody.userID;
    let itemsSold = alibay.allItemsSold(userID);
    let resp = {
        itemsSold: itemsSold,
        firstName: users[userID].firstName,
        lastName: users[userID].lastName,
        username: users[userID].username
    }

    if(itemsSold === undefined){
        resp = {
            firstName: users[userID].firstName,
            lastName: users[userID].lastName 
        }
        res.send(JSON.stringify(resp))
    }

    res.send(JSON.stringify(resp))

});

app.post('/itemsBought', (req, res) =>{
    let parsedBody = JSON.parse(req.body.toString());
    let userID = parsedBody.userID;
    let itemsBought = alibay.allItemsBought(userID);
    if(itemsBought=== undefined){
        return;
    }
    res.send(JSON.stringify(itemsBought))

});
    
//     res.send();
// });

app.post('/buyItem', (req, res) =>{
    let parsedBody = JSON.parse(req.body.toString());
    let itemID = parsedBody.itemID;
    let userID = parsedBody.userID;
    
    alibay.buy(userID, itemID);

    let confNum = genUniqueID();
    res.send({confirmationNum: confNum});

});

app.get('/searchItems', (req, res) => {
    let searchTerm = req.query.search;
    let searchItems = alibay.searchForListings(searchTerm); 
    
    res.send(JSON.stringify(searchItems));
});

//profile page??
app.post('/profile', (req, res) =>{
    let parsedBody = JSON.parse(req.body.toString());
    let userID = parsedBody.userID;
    let itemsBought = alibay.allItemsBought(userID);
    let itemsSold = alibay.allItemsSold(userID);
    let firstName = users[userID].firstName;
    let lastName = users[userID].lastName;
    
    let resp = {
        itemsBought: itemsBought,
        itemsSold: itemsSold,
        firstName: firstName,
        lastName: lastName
    };
    
    
    
    res.send(JSON.stringify(resp));
});

app.listen(4000, () => console.log('Listening on port 4000!'))
