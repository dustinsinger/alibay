import React, { Component } from 'react';
import './App.css';
import './Homepage.css';
import anime from 'animejs';
import { Redirect, Link, withRouter } from "react-router-dom";
import homeDecoration from './images/homedeco1.png';
import clothes from './images/clothing1.png';
import electronics from './images/electro1.png';
import cars from './images/motors1.png';
import printedWork from './images/printwork1.png';
import truck from './images/delivery-truck.png';
import money from './images/money-back.png';
import service from './images/24-hours.png';


let leftButton = null;
let rightButton = null;

class Homepage extends Component {
    constructor() {
        super();
        this.state = {
            searchQuery: "",
            recentItems: [],
            responseMessage: {
                status: "Item Successfully Added",
                newItem: {
                    photo: "",
                    category: "",
                    price: "",
                    description: "",
                    userID: "",
                    name: ""
                }
            },
        };
        this.searchQuery = this.searchQuery.bind(this);
        this.handleMoveLeftSubmit = this.handleMoveLeftSubmit.bind(this);
        this.handleMoveRightSubmit = this.handleMoveRightSubmit.bind(this);
        this.updateRecentItems = this.updateRecentItems.bind(this);
        this.logout = this.logout.bind(this);
        this.onSearch = this.onSearch.bind(this);

    }

    updateRecentItems() {
        /*  fetch('/homepage', {
              method: 'GET',
              body: undefined
          })
          .then(response => response.text())
          .then(resBod => {
              let temp = JSON.parse(resBod);
              this.setState({responseMessage: temp});
  
          
              
          })
  
  
          if(this.state.responseMessage.status === "Item Successfully Added"){
              return(
                  <div id="recentlyAdded">
                      <div id="newPhoto">{this.state.responseMessage.newItem.photo}</div>
                      <p id="newCat">{this.state.responseMessage.newItem.category}</p>
                      <p id="newPrice">{"$" + this.state.responseMessage.newItem.price}</p>
                      <p id="newDescr">{this.state.responseMessage.newItem.description}</p>
                      <p id="newID">{this.state.responseMessage.newItem.userID}</p>
                  </div>
              );
          }*/
    }


    handleMoveLeftSubmit(event) {
        event.preventDefault();
        document.getElementById("app").style.left = document.getElementById("app").style.left + '-50vw'
    }

    handleMoveRightSubmit(event) {
        event.preventDefault();
    }

    searchQuery(e) {
        this.setState({ searchQuery: e.target.value });
    }

    logout() {
        this.props.logout();
    }

    componentDidMount(){
        if(localStorage.getItem("userID") == null){
            return(<Redirect to="/"></Redirect>)
        }
    }

    onSearch(){
        this.props.history.push({ pathname: "/itemSearch", search: "?search=" + this.state.searchQuery })
    }

    render() {
        // if (localStorage.getItem("userid") === null) {
        //     return (<Redirect to="/"></Redirect>)
        // }
        return (
            <div className="homepage comp">
                <div id="headerHP">
                    <ul>
                        <li><div>{"FREE Shipping on all orders across Canada!"}</div></li>
                        <li>{"Alibay"}</li>
                    </ul>
                    <form onSubmit={this.onSearch}>
                        <input type="text" placeholder="Search for item" onChange={this.searchQuery} value={this.state.searchQuery} id="searchItem" />
                        <input type="submit" className="search-button" value="Search"></input>
                    </form>
                    <Link className="my-account" to="/profile">My Account</Link>
                    <button className="logout" onClick={this.logout}>Logout</button>
                </div>


                <div id="slideshow">


                    <div id="app">
                        <div style={{ display: "block" }}>
                            <div id="overlay1_hp">
                                <Link to={{ pathname: "/itemSearch", search: "?search=homedecoration" }}>
                                    <img id="homedecor_hp" src={homeDecoration} alt="placeholder image" />
                                </Link>
                            </div>
                        </div>


                        <div style={{ display: "block" }}>
                            <div id="overlay2_hp">
                                <Link to={{ pathname: "/itemSearch", search: "?search=clothing" }}>
                                    <img id="clothing_hp" src={clothes} alt="placeholder image" />
                                </Link>
                            </div>
                        </div>



                        <div style={{ display: "block" }}>
                            <div id="overlay3_hp">
                                <Link to={{ pathname: "/itemSearch", search: "?search=electronics" }}>
                                    <img id="electro_hp" src={electronics} alt="placeholder image" />
                                </Link>
                            </div>
                        </div>


                        <div id="overlay4_hp">
                            <div style={{ display: "block" }}>
                                <Link to={{ pathname: "/itemSearch", search: "?search=cars" }}>
                                    <img id="motors_hp" src={cars} alt="placeholder image" />
                                </Link>
                            </div>
                        </div>


                        <div id="overlay5_hp">
                            <div style={{ display: "block" }}>
                                <Link to={{ pathname: "/itemSearch", search: "?search=printedworks" }}>
                                    <img id="printedwork_hp" src={printedWork} alt="placeholder image" />
                                </Link>
                            </div>
                        </div>
                    </div>

                    <form>
                        <input type="submit" id="moveLeft" value="<" onClick={this.handleMoveLeftSubmit} />
                        <input type="submit" id="moveRight" value=">" onClick={this.handleMoveRightSubmit} />
                    </form>

                </div>

                <div id="about_hp">
                    <ul>
                        <li>{"Alibay"}</li>
                        <li>{"About Us"}</li>
                        <li>{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dapibus, urna in consectetur cursus, quam augue porttitor quam, eu lacinia risus libero in felis. In mattis quam non arcu molestie, a tempor felis pellentesque. Aliquam erat volutpat. Nulla lectus ipsum, sagittis vel mollis tempor, iaculis eu leo."}</li>
                    </ul>
                </div>

                <div id="services">
                    <div id="section1">
                        <p>{"Our Services"}</p>
                    </div>
                    <div id="servi1">
                        <ul>
                            <li><img src={service} /></li>
                            <li>{"24/7 Service"}</li>
                            <li>{"Lorem ipsum dolor sit amet, consectetur adipiscing elit."}</li>
                        </ul>
                    </div>
                    <div id="servi2">
                        <ul>
                            <li><img src={truck} /></li>
                            <li>{"Free Delivery"}</li>
                            <li>{"Lorem ipsum dolor sit amet, consectetur adipiscing elit."}</li>
                        </ul>
                    </div>
                    <div id="servi3">
                        <ul>
                            <li><img src={money} /></li>
                            <li>{"Money Back"}</li>
                            <li>{"Lorem ipsum dolor sit amet, consectetur adipiscing elit."}</li>
                        </ul>
                    </div>
                </div>

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>

                
                {
                    document.addEventListener("DOMContentLoaded",
                        function () {

                        }
                    )
                }
            </div>
        );
    }
}

export default withRouter(Homepage);