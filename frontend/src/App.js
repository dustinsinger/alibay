import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, withRouter, Redirect } from 'react-router-dom';
import './App.css';
import Login from './Login.js';
import CreateAccount from './CreateAccount.js';
import Homepage from './HomePage.js';
import BuyItem from './BuyItem.js'
import Confirmation from './Confirmation.js';
import AddItem from './AddItem.js';
import Profile from './Profile.js';
import ItemsList from './ItemsList.js'
import ItemSearch from './ItemSearch.js'
import PageNotFound from './PageNotFound.js'

class App extends Component {
  constructor() {
    super();
    this.returnLogin = this.returnLogin.bind(this);
    this.returnBuyItem = this.returnBuyItem.bind(this);
    this.returnCreateAccount = this.returnCreateAccount.bind(this);
    this.returnHomepage = this.returnHomepage.bind(this);
    this.returnConfirmation = this.returnConfirmation.bind(this);
    this.returnProfile = this.returnProfile.bind(this);
    this.returnAddItem = this.returnAddItem.bind(this);
    this.returnItemsList = this.returnItemsList.bind(this)
    this.pageNotFound = this.pageNotFound.bind(this);
    this.setLoggedInState = this.setLoggedInState.bind(this);
    this.logout = this.logout.bind(this)
    this.state = {
      userID: localStorage.getItem("userID")
    }
  }

  setLoggedInState(userID) {
    localStorage.setItem("userID", userID);
    this.setState({userID: userID })
  }

  returnLogin() {
    return (<Login setLoggedInState={this.setLoggedInState} />)
  }

  returnCreateAccount() {
    return (<CreateAccount />)
  }

  returnHomepage() {
    return (<Homepage logout={this.logout} />)
  }

  returnConfirmation = withRouter(props =>
    <Confirmation itemID={props.location.state.itemID} userID={this.state.userID}/>
  )

  returnProfile() {
    return (<Profile userID={this.state.userID} />)
  }

  returnAddItem() {
    return (<AddItem userID={this.state.userID} />)
  }

  returnItemsList(routerData) {
    let type = routerData.match.params.type
    return (<ItemsList type={type} userID={this.state.userID} />)
  }

  returnItemSeach() {
    return (<ItemSearch />)
  }

  returnBuyItem() {
    return (<BuyItem />);
  }
  pageNotFound() {
    return (<PageNotFound />)
  }

  logout(){
    localStorage.clear();
    window.location.reload();
  }

  render() {
    if (localStorage.getItem("userID") === null) {
      return (
        <BrowserRouter>
          <div>
            <Switch>
              <Redirect from="/homepage" to="/"></Redirect>
              <Route exact={true} path="/" render={this.returnLogin}></Route>
              <Route exact={true} path="/signUp" render={this.returnCreateAccount}></Route>
              <Route render={this.pageNotFound}></Route>
            </Switch>
          </div>
        </BrowserRouter>
      );
    }

    return(
    <BrowserRouter>
        <div>
          <Switch>
            <Redirect exact={true} from="/" to="/homepage"></Redirect>
            <Redirect exact={true} from="/signup" to="/homepage"></Redirect>
            <Route exact={true} path="/homepage" render={this.returnHomepage}></Route>
            <Route exact={true} path="/confirmation" component={this.returnConfirmation}></Route>
            <Route exact={true} path="/profile" render={this.returnProfile}></Route>
            <Route exact={true} path="/addItem" render={this.returnAddItem}></Route>
            <Route exact={true} path="/itemSearch" render={this.returnItemSeach}></Route>
            <Route exact={true} path="/buyItem" render={this.returnBuyItem}></Route>
            <Route render={this.pageNotFound}></Route>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
