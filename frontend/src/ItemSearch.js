import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import './ItemSearch.css';

class ItemSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }


    componentDidMount() {
        fetch('/searchItems' + window.location.search)
            .then(x => x.text())
            .then(response => {
                let items = JSON.parse(response)
                this.setState({ items: items })
            })
    }

    displaySearchedItems(){
        if(this.state.items.length === 0){
            return(
                <p>{"- No Items Available in this category -"}</p>
            );
        }
        else{
            return(
                <div>{this.state.items.map((x, i) => 
                    <div key={i}>
                        <div className="nameItemsToBuy"><span>{"Name: "}</span> {x.name}</div>
                        <div className="priceItemsToBuy"><span>{"Price: "}</span> {"$"+ x.price}</div>
                        <div className="descrItemsToBuy"><span>{"Desciption: "}</span>{x.description}</div>
                        <Link to={{ pathname: "/confirmation", state: { itemID: x.itemID}}} className="buyThisItem">Buy Item</Link>
                    </div>)
                }
                </div>
            )
        }
    }

    buyItem(itemID) {
        console.log(itemID);
    }

    render() {
        return (
            <div className="App comp">

                <div id="headerIS">
                    <ul>
                        <li><div>{"FREE Shipping on all orders across Canada!"}</div></li>
                        <li>{"Alibay"}</li>
                    </ul>
                    <Link id="my-accountIS" to="/profile">My Account</Link>
                    <button id="logoutIS" onClick={this.logout}>Logout</button>
                </div>

                <div id="itemsInCategory">
                    {this.displaySearchedItems()}
                </div>    

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>

            </div>
        );
    }
}

export default ItemSearch;