import React, {Component} from 'react';
import './App.css';
import anime from 'animejs';
import './BuyItem.css';

let canPlaceOrder = true;

class BuyItem extends Component{
    constructor(){
        super();
        this.state = {
            clientFirstName: '',
            clientLastName: '',
            clientAddress: '',
            clientCity: '',
            clientProvince: '',
            clientPostalCode: '',
            clientPhone: '',
            firstNameErrorMessage: '',
            lastNameErrorMessage: '',
            addressErrorMessage: '',
            cityErrorMessage: '',
            provinceErrorMessage: '',
            postalCodeErrorMessage: '',
            phoneErrorMessage: ''
        }

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleProvinceChange = this.handleProvinceChange.bind(this);
        this.handlePostCodeChange = this.handlePostCodeChange.bind(this);
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.preventOrder = this.preventOrder.bind(this);
        this.updateErrorMessages = this.updateErrorMessages.bind(this);

    }

    handleFirstNameChange(event){
        event.preventDefault();
        this.setState({clientFirstName: event.target.value});
    }

    handleLastNameChange(event){
        event.preventDefault();
        this.setState({clientLastName: event.target.value});
    }

    handleAddressChange(event){
        event.preventDefault();
        this.setState({clientAddress: event.target.value});
    }

    handleCityChange(event){
        event.preventDefault();
        this.setState({clientCity: event.target.value});
    }

    handleProvinceChange(event){
        event.preventDefault();
        this.setState({clientProvince: event.target.value});
    }

    handlePostCodeChange(event){
        event.preventDefault();
        this.setState({clientPostalCode: event.target.value});
        
    }
    handlePhoneChange(event){
        event.preventDefault();
        this.setState({clientPhone: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        if(this.preventOrder() === false){
            return ;
        }
        else{

        }
    }

    preventOrder(){
    

        //Prevent user from completing transaction if firstname is not specified 
        if(this.state.clientFirstName.length === 0){
            canPlaceOrder = false;
            this.setState({firstNameErrorMessage: 'You need to enter the firstname before placing the order. Try again'});
        }

        //Prevent user from completing the transaction if lastname is not specified
        if(this.state.clientLastName.length === 0){
            canPlaceOrder = false;
            this.setState({lastNameErrorMessage: 'You need to enter the lastname before placing the order. Try again'});
        }

        //Prevent user from completing the transaction if address is not specified
        if(this.state.clientAddress.length === 0){
            canPlaceOrder = false;
            this.setState({addressErrorMessage: 'You need to enter the address before placing the order. Try again'});
        }

        //Prevent user from completing the transtation if city is not specified
        if(this.state.clientCity.length === 0){
            canPlaceOrder = false;
            this.setState({cityErrorMessage: 'You need to enter the city before placing the order. Try again'});
        }

        //Prevent user from completing the transaction if province is not specified
        if(this.state.clientProvince.length === 0){
            canPlaceOrder = false;
            this.setState({provinceErrorMessage: 'You select a province. Try again'});
        }

        //Prevent user from completing the transaction if postal code is not specified
        if(this.state.clientPostalCode.length === 0){
            canPlaceOrder = false;
            this.setState({postalCodeErrorMessage: 'You need to enter the postal code before placing the order. Try again'});
        }

        //Prevent user from completing the transaction if phone number is not specified
        if(this.state.clientPhone.length === 0){
            canPlaceOrder = false;
            this.setState({phoneErrorMessage: 'You need to enter the phone number before placing the order. Try again'})
        }

        //Prevent user from completing the transaction if postal code is not valid; i.e. if it does not follow the A1A 1A1 rule
            //remove white space from postal code
            let arr = this.state.clientPostalCode.trim().split("");
            let temp = arr;
            console.log(temp);

        

        return canPlaceOrder;
    }
























    updateErrorMessages(){
        
    }





    render(){
        return(
        
        <div>
            <form>
                <input type="text" placeholder="First Name" />
                <p className="errors">{this.state.firstNameErrorMessage}</p>

                <input type="text" onChange={this.handleLastNameChange} placeholder="Last Name"  />
                <p className="errors">{this.state.lastNameErrorMessage}</p>

                <input type="text" onChange={this.handleAddressChange} placeholder="Address"    />
                <p className="errors">{this.state.addressErrorMessage}</p>

                <input type="text" onChange={this.handleCityChange} placeholder="City"       />
                <p className="errors">{this.state.cityErrorMessage}</p>

                <input type="text" onChange={this.handleProvinceChange} placeholder="Province"   />
                <p className="errors">{this.state.provinceErrorMessage}</p>

                <input type="text" onChange={this.handlePostCodeChange} placeholder="Postal Code"    />
                <p className="errors">{this.state.postalCodeErrorMessage}</p>

                <input type="text" onChange={this.handlePhoneChange} placeholder="Phone"      />
                <p className="errors">{this.state.phoneErrorMessage}</p>

                <input type="submit" value="Place Order" onClick={this.handleSubmit}    />
            </form>
        </div>
        
    );
    }
}

export default BuyItem;