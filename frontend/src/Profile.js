import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import './Profile.css';
import anime from 'animejs';
import AddItem from './AddItem';

let block = {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
}

let canAddNewItem = true;

let nameInput = null;
let categoryInput = null;
let descrInput = null;
let priceInput = null;
class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userID: this.props.userID,
            firstName: 'Sponge',
            lastName: 'Bob',
            username: 'pineapple@bikiniBottom.com',
            address: '1234 rue HappyLand',
            city: 'Montreal',
            province: 'Quebec',
            code: 'H7W 9V7',
            itemsBought: [],
            itemsSold: [],
            responseMessage: "",
            itemNameErrorMessage: "",
            itemCategoryErrorMessage: "",
            itemDescrErrorMessage: "",
            itemPriceErrorMessage: "",
            newItemName: '',
            newItemCategory: '',
            newItemDescr: '',
            newItemPrice: ''

        }
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleProvinceChange = this.handleProvinceChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.displayItemsBought = this.displayItemsBought.bind(this);
        this.displayItemsSold = this.displayItemsSold.bind(this);
        this.allowAddingItems = this.allowAddingItems.bind(this);
    }

    handleAddressChange(event) {
        event.preventDefault();
        document.getElementById("editAddress").style.opacity = 0;
        document.getElementById("addressChange").style.opacity = 1;
    }

    handleFirstNameChange(event) {
        event.preventDefault();
        this.setState({ firstName: event.target.value });
    }

    handleLastNameChange(event) {
        event.preventDefault();
        this.setState({ lastName: event.target.value });
    }

    handleCityChange(event) {
        event.preventDefault();
        this.setState({ city: event.target.value });
    }

    handleProvinceChange(event) {
        event.preventDefault();
        this.setState({ province: event.target.value });
    }

    handleCodeChange(event) {
        event.preventDefault();
        this.setState({ code: event.target.value })
    }

    handleSave(event) {
        event.preventDefault();
        document.getElementById("addressChange").style.opacity = 0;
        document.getElementById("editAddress").style.opacity = 1;
    }

    displayItemsBought() {

        if (this.state.itemsBought.length === 0) {
            return (
                <p id="noItemBought">{"- No Items Bought -"}</p>
            );
        }
        else {
            return (
                <div>{this.state.itemsSold.map((x, i) => <div key={i}>{x.name}, {x.price}, {x.description}</div>)}</div>
            );
        }
    }

    displayItemsSold() {

        if (this.state.itemsSold.length === 0) {
            return (
                <p id="noItemSold">{"- No Items Sold -"}</p>
            );
        }
        else {
            return (
                <div>{this.state.itemsSold.map((x, i) => <div key={i}>{x.name}, {x.price}, {x.description}</div>)}</div>
            );
        }
    }

    allowAddingItems() {
        document.getElementById("addItemButton").style.opacity = 0;
        return (<AddItem id="addItemComponent" />);
    }



    componentDidMount() {
        {
            fetch('/itemsSold', {
                method: 'POST',
                body: JSON.stringify({ userID: this.state.userID })
            })
                .then(x => x.text())
                .then(response => {
                    let resp = JSON.parse(response)
                    this.setState({ 
                        itemsSold: resp.itemsSold,
                        firstName: resp.firstName,
                        lastName: resp.lastName,
                        username: resp.username
                     })
                })
        }

        fetch('/itemsBought', {
            method: 'POST',
            body: JSON.stringify({ userID: this.state.userID })
        })
            .then(x => x.text())
            .then(response => {
                let allItemsBought = JSON.parse(response)
                this.setState({ itemsBought: allItemsBought })
            })
    }


    render() {
        return (
            <div id="profilePage">
                <div id="headerPro">
                    <ul>
                        <li>
                            <div>{"FREE Shipping on all orders across Canada!"}</div>
                        </li>
                        <li>{"ALIBAY"}</li>
                        <li></li>
                    </ul>
                </div>

                <Link className="homepage-button" to="/homepage">Home</Link>

                <p id="greeting">{"Hi " + this.state.firstName + "!"}</p>

                <div id="persoInfo">
                    <ul>
                        <li>{"Personal Info"}</li>
                        <li><div></div></li>
                        <li>{this.state.firstName + " " + this.state.lastName}</li>
                        <li>{this.state.username}</li>
                    </ul>
                </div>

                <div id="addressInfo">
                    <ul>
                        <li>{"Address"}</li>
                        <li><div></div></li>
                        <li>
                            <form id="addressChange">
                                {"First Name: "}<input type="text" onChange={this.handleFirstNameChange} value={this.state.firstName} /><p></p>
                                {"Last Name: "}<input type="text" onChange={this.handleLastNameChange} value={this.state.lastName} /><p></p>
                                {"Address: "}<input type="text" onChange={this.handleAddressChange} value={this.state.address} /><p></p>
                                {"City: "}<input type="text" onChange={this.handleCityChange} value={this.state.city} /><p></p>
                                {"Province: "}<input type="text" onChange={this.handleProvinceChange} value={this.state.province} /><p></p>
                                {"Postal Code: "}<input type="text" onChange={this.handleCodeChange} value={this.state.code} /><p></p>
                                <p></p><input type="submit" onClick={this.handleSave} value="Save Changes" />
                            </form>
                        </li>
                        <li><input type="submit" value="EDIT" onClick={this.handleAddressChange} id="editAddress" /></li>
                    </ul>
                </div>

                <div id="itemsSold">
                    <ul>
                        <li>{"Items Sold"}</li>
                        <li><div></div></li>
                        <li>{this.displayItemsSold()}</li>
                    </ul>
                </div>

                <div id="itemsBought">
                    <ul>
                        <li>{"Items Bought"}</li>
                        <li><div></div></li>
                        <li>{this.displayItemsBought()}</li>
                    </ul>
                </div>


                <div id="buyItem">
                    <ul>
                        <li>{"Add Item"}</li>
                        <li><div></div></li>
                    </ul>

                    <Link to="/AddItem" id="linkToAddItem">{"Add Item"}</Link>
                </div>

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Profile;