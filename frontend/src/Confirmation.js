import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './App.css';
import './Confirmation.css';

class Confirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userID: this.props.userID,
            confNum: ""
        }
    }

    componentDidMount() {
        fetch('/buyItem', {
            method: 'POST',
            body: JSON.stringify({ userID: this.props.userID, itemID: this.props.itemID })
        })
            .then(x => x.text())
            .then(res => {
                let body = JSON.parse(res);
                let confNum = body.confirmationNum;
                this.setState({ confNum: confNum })
            })
    }

    render() {
        return (
            <div className="App comp">

                <div id="headerConfirm">
                    <ul>
                        <li>
                            <div>{"FREE Shipping on all orders across Canada!"}</div>
                        </li>
                        <li>{"ALIBAY"}</li>
                        <li></li>
                    </ul>
                </div>

                <h3>{"Item Successfully Purchased"}</h3>
                <div><h4>"Confirmation Number:" {this.state.confNum}</h4></div>
                <Link className="homepage-button" to="/homepage">Back to homepage</Link>

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Confirmation;