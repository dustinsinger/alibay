import React, { Component } from 'react';
import { BrowserRouter, Link, Route, Redirect } from 'react-router-dom';
import anime from 'animejs';
import './App.css';
import './Login.css';
import CreateAccount from './CreateAccount';

let canSubmitLoginForm = true;

var lineOne = null;
var lineTwo = null;
var lineThree = null;
var lineFour = null;
var userNameInput = null;
var passwordInput = null;
var loginInput = null;
var title = null;
var slogan = null;
var counter = 0;
var letter1 = null;
var letter2 = null;
var letter3 = null;
var letter4 = null;
var letter5 = null;
var letter6 = null;

class Login extends Component {
    constructor() {
        super()
        this.state = {
            userID: '',
            username: '',
            password: '',
            userNameErrorMessage: '',
            passwordErrorMessage: '',
            responseMessage: { success: false, reason: 'invalid username' }
        }
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.preventLoginSubmit = this.preventLoginSubmit.bind(this);
        this.updateErrorMessages = this.updateErrorMessages.bind(this);
        this.anim1 = this.anim1.bind(this);
        this.anim2 = this.anim2.bind(this);
    }

    handleUsernameChange(event) {
        event.preventDefault();
        this.setState({ username: event.target.value });
        this.updateErrorMessages();
    }

    handlePasswordChange(event) {
        event.preventDefault();
        this.setState({ password: event.target.value });
        this.updateErrorMessages();
    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.preventLoginSubmit() === false) {
            return;
        }
        else {
            fetch('/login', {
                method: 'POST',
                body: JSON.stringify({ username: this.state.username, password: this.state.password })
            })
                .then(response => response.text())
                .then(resBod => {
                    let temp = JSON.parse(resBod);
                    if (temp.success === false) {
                        this.setState({ responseMessage: { success: false } })
                    }
                    else {
                        this.setState({ userID: temp.userID })
                        this.props.setLoggedInState(temp.userID)
                    }
                })

            if (this.state.responseMessage.success === false) {
                if (this.state.responseMessage.reason === "invalid username") {
                    this.setState({ userNameErrorMessage: "Invalid username. Try again" });
                    document.getElementById("email_login").value = "";      //empty the input textbox
                    this.setState({ username: '' });

                }
                else if (this.state.responseMessage.reason === "invalid password") {
                    this.setState({ passwordErrorMessage: "Invalid password. Try again" });
                    document.getElementById("password_login").value = "";   //empty the input textbox
                    this.setState({ password: '' });
                }
            }
        }
    }

    // renderCreateAccount() {
    //     return (<CreateAccount />);
    // }

    /*
        Function that prevents hanleSubmit to be executed in the event that the username or the password are not valid
    */
    preventLoginSubmit() {
        //Return error message if user did not write any input in username text field
        if (this.state.username.length === 0) {
            canSubmitLoginForm = false;
            //document.getElementById('email_login').value = "Invalid Username";
            document.getElementById('email_login').style.color = 'red';
            document.getElementById('email_login').style.fontSize = '11px';
            document.getElementById('email_login').style.borderColor = 'red';
            this.setState({ userNameErrorMessage: 'You need to enter a username' });

        }

        //Return error message if user did not write any inout in password field
        if (this.state.password.length === 0) {
            canSubmitLoginForm = false;
            ///document.getElementById('password_login').value = "Invalid Password";
            document.getElementById('password_login').style.color = 'red';
            document.getElementById('password_login').style.fontSize = '11px';
            document.getElementById('password_login').style.borderColor = 'red';
            this.setState({ passwordErrorMessage: 'You need to enter a password' });

        }

        return canSubmitLoginForm;
    }

    updateErrorMessages() {
        if (this.state.username.length > 0) {
            this.setState({ userNameErrorMessage: '' });
        }
        if (this.state.password.length > 0) {
            this.setState({ passwordErrorMessage: '' });
        }
    }
    componentDidMount(){
        this.anim1();
        this.anim2();
    }


    anim1() {
        lineOne = anime({
            targets: document.getElementById('line1_login'),
            height: 200,
            autoplay: true,
            delay: 2900,
            duration: 550
        })

        lineTwo = anime({
            targets: document.getElementById('line2_login'),
            width: 500,
            delay: 3050,
            duration: 550
        })

        lineThree = anime({
            targets: document.getElementById('line3_login'),
            height: 300,
            delay: 3200,
            duration: 550
        })

        lineFour = anime({
            targets: document.getElementById('line4_login'),
            width: 150,
            delay: 3350,
            duration: 550
        })

        userNameInput = anime({
            targets: document.getElementById('email_login'),
            delay: 2700,
            duration: 600,
            translateY: -150
        })

        passwordInput = anime({
            targets: document.getElementById('password_login'),
            delay: 2700,
            duration: 600,
            translateY: -150
        })

        loginInput = anime({
            targets: document.getElementById('login'),
            delay: 2700,
            duration: 600,
            translateY: -150
        })

        letter1 = anime({
            targets: document.getElementById('letter1'),
            opacity: 1,
            delay: 500,
            duration: 70
        })

        letter2 = anime({
            targets: document.getElementById('letter2'),
            opacity: 1,
            delay: 580,
            duration: 70
        })

        letter3 = anime({
            targets: document.getElementById('letter3'),
            opacity: 1,
            delay: 660,
            duration: 70
        })

        letter4 = anime({
            targets: document.getElementById('letter4'),
            opacity: 1,
            delay: 740,
            duration: 70
        })

        letter5 = anime({
            targets: document.getElementById('letter5'),
            opacity: 1,
            delay: 820,
            duration: 70
        })

        letter6 = anime({
            targets: document.getElementById('letter6'),
            opacity: 1,
            delay: 900,
            duration: 70
        })

        slogan = anime({
            targets: document.getElementById('slogan_login'),
            opacity: 1,
            delay: 1480,
            duration: 10000
        })

    }

    anim2() {
        document.getElementById('email_login').addEventListener('focus',
            function () {
                counter++;
                userNameInput = anime({
                    targets: document.getElementById('email_login'),
                    borderColor: '#ffc3bc',
                    color: '#151515'
                })
            }
        ),
            document.getElementById('password_login').addEventListener('focus',
                function () {
                    counter++;
                    passwordInput = anime({
                        targets: document.getElementById('password_login'),
                        borderColor: '#ffc3bc',
                        color: '#151515'
                    })
                }
            ),
            document.getElementById('password_login').addEventListener('focus',
                function () {
                    counter++;
                    if (counter > 2) {
                        loginInput = anime({
                            targets: document.getElementById('login'),
                            backgroundColor: '#d8685b'
                        })
                    }
                }
            )

    }


    render() {
        if (this.state.userID != "") {
            return (<Redirect to="/homepage"></Redirect>)
        }
        return (
            <div className="loginPage comp">
                <div className="aesthetic">
                    <div className="login_overlay">
                        <div id="title_login">
                            <span id="letter1">A</span>
                            <span id="letter2">L</span>
                            <span id="letter3">I</span>
                            <span id="letter4">B</span>
                            <span id="letter5">A</span>
                            <span id="letter6">Y</span>
                        </div>
                        <p id="slogan_login">Once you try it,<br />you adopt it</p>
                        <Link className="signup" style={{ zIndex: 1 }} to="/signUp">SIGN UP</Link>
                        <div id="line1_login"></div>
                        <div id="line2_login"></div>
                        <div id="line3_login"></div>
                        <div id="line4_login"></div>
                    </div>
                </div>

                <form>
                    <input type="text" onChange={this.handleUsernameChange} placeholder="Email" id="email_login" />
                    <p id="usernameError">{this.state.userNameErrorMessage}</p>
                    <input type="text" onChange={this.handlePasswordChange} placeholder="Password" id="password_login" />
                    <p id="passwordError">{this.state.passwordErrorMessage}</p>
                    <input type="submit" onClick={this.handleSubmit} value="LOG IN" id="login" />
                </form>
            </div>
        );
    }



















    animateLine1() {
        document.getElementById
    }
}

export default Login;