import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import './App.css';
import './AddItem.css';
import anime from 'animejs';

let block = {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
}

let canAddNewItem = true;

let nameInput = null;
let categoryInput = null;
let descrInput = null;
let priceInput = null;

class AddItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userID: this.props.userID,
            itemName: "",
            itemCategory: "",
            itemDesciption: "",
            itemPrice: "",
            // itemImage: "",
            responseMessage: "",
            itemNameErrorMessage: "",
            itemCategoryErrorMessage: "",
            itemDescrErrorMessage: "",
            itemPriceErrorMessage: "",
        }
        this.addItem = this.addItem.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.handlePrice = this.handlePrice.bind(this);
        this.preventAddingNewItem = this.preventAddingNewItem.bind(this);
        this.updateErrorMessages = this.updateErrorMessages.bind(this);
        // this.handleImage = this.handleImage.bind(this);
    }

    handleName(e) {
        this.setState({ itemName: e.target.value });
    }
    handleCategory(e) {
        this.setState({ itemCategory: e.target.value });
    }
    handleDescription(e) {
        this.setState({ itemDesciption: e.target.value });
    }
    handlePrice(e) {
        this.setState({ itemPrice: e.target.value })
    }

    preventAddingNewItem(){
        //Prevent adding item if name is not specified
        if(this.state.itemName.length === 0){
            canAddNewItem = false;
            this.setState({itemNameErrorMessage: "You need to enter the name of the item. Try Again"});
            document.getElementById("itemName").style.borderColor = 'red';
        }

        //Prevent adding item if category is not specified
        if(this.state.itemCategory.length === 0){
            canAddNewItem = false;
            this.setState({itemCategoryErrorMessage: "You need to enter a category for the item. Try Again"});
            document.getElementById("itemCategory").style.borderColor = 'red';
        }

        //Prevent adding item if description is not given
        if(this.state.itemDesciption.length === 0){
            canAddNewItem = false;
            this.setState({itemDescrErrorMessage: "You need to enter a description for the item. Try Again"});
            document.getElementById("itemDescr").style.borderColor = 'red';
        }

        //Prevent addding item if price is not specified
        if(this.state.itemPrice.length === 0){
            canAddNewItem = false;
            this.setState({itemPriceErrorMessage: "You need to enter a price for the item. Try Again"});
            document.getElementById("itemPrice").style.borderColor = 'red';
        }

        return canAddNewItem;
    }

    updateErrorMessages(){

    }
    // handleImage(e) {
    //     this.setState({ itemImage: e.target.value });
    // }


    
    addItem(e) {
        e.preventDefault();
        if(this.preventAddingNewItem() === false){
            return;
        }
        this.setState({
            itemName: "",
            itemCategory: "",
            itemDesciption: "",
            itemPrice: "",
        })
        fetch('/addItem', {
            method: 'POST',
            body: JSON.stringify({
                userID: this.state.userID,
                name: this.state.itemName,
                category: this.state.itemCategory,
                description: this.state.itemDesciption,
                price: parseFloat(this.state.itemPrice),
            })
        })
            .then(res => res.text())
            .then(body => {
                this.setState({ responseMessage: body })
            })
    }

    render() {
        return (
            <div className="App comp">

                <div id="headerAdd">
                    <ul>
                        <li>
                            <div>{"FREE Shipping on all orders across Canada!"}</div>
                        </li>
                        <li>{"ALIBAY"}</li>
                        <li></li>
                    </ul>
                </div>

                <div>{this.state.responseMessage}</div>
                <form className="add-item-form" onSubmit={this.addItem}>
                    <input className="input" id = "itemName" style={block} type="text" placeholder="Enter Item Name" onChange={this.handleName} value={this.state.itemName} />
                    <p id="nameError">{this.state.itemNameErrorMessage}</p>

                    <input className="input" id="itemCategory" style={block} type="text" placeholder="Enter Item Category" onChange={this.handleCategory} value={this.state.itemCategory} />
                    <p id="categoryError">{this.state.itemCategoryErrorMessage}</p>

                    <input className="input" id="itemDescr" style={block} type="text" placeholder="Enter Item Description" onChange={this.handleDescription} value={this.state.itemDesciption} />
                    <p id="descrError">{this.state.itemDescrErrorMessage}</p>

                    <input className="input" id="itemPrice" style={block} type="text" placeholder="Enter Item Price" onChange={this.handlePrice} value={this.state.itemPrice} />
                    <p id="priceError">{this.state.itemPriceErrorMessage}</p>    

                    {/* <input style={block} type="text" placeholder="Enter Image URL" onChange={this.handleImage} value={this.state.itemImages}/> */}
                    <input className="submit" style={block} type="submit" value="Submit" />
                    
                </form>
                <Link className="home" to="/homepage">Home</Link>

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>


                {
                    document.addEventListener("DOMContentLoaded", 
                        function(){
                            document.getElementById('itemName').addEventListener('focus',
                                function(){
                                    nameInput = anime({
                                        targets: document.getElementById('itemName'),
                                        borderColor: '#ffc3bc'  
                                    })
                                }
                            )

                            document.getElementById('itemCategory').addEventListener('focus',
                                function(){
                                    categoryInput = anime({
                                        targets: document.getElementById('itemCategory'),
                                        borderColor: '#ffc3bc'
                                    })
                                }
                            )

                            document.getElementById('itemDescr').addEventListener('focus',
                                function(){
                                    nameInput = anime({
                                        targets: document.getElementById('itemDescr'),
                                        borderColor: '#ffc3bc'  
                                    })
                                }
                            )

                            document.getElementById('itemPrice').addEventListener('focus',
                                function(){
                                    categoryInput = anime({
                                        targets: document.getElementById('itemPrice'),
                                        borderColor: '#ffc3bc'
                                    })
                                }
                            )
                        }
                    ) 
                }
            </div>
        );
    }
}

export default AddItem;