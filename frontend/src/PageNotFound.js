import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './App.css';
import './PageNotFound.css'

class PageNotFound extends Component {

    render() {
        return (
            <div className="App comp">
                <div id="headerPNF">
                    <ul>
                        <li>
                            <div>{"FREE Shipping on all orders across Canada!"}</div>
                        </li>
                        <li>{"ALIBAY"}</li>
                        <li></li>
                    </ul>
                </div>

                <h3>404 Page Not Found</h3>
                <h4>Whoops, looks like someone's lost</h4>
                <button onClick={() => {window.history.back()}}>Go back</button>

            <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>    
                
            </div>
        );
    }
}

export default PageNotFound;