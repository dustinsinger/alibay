import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import './App.css';
import './CreateAccount.css';
import anime from 'animejs';

let canSubmitAccountCreationForm = true;
let firstNameInput = null;
let lastNameInput = null;
let emailInput = null;
let passwordInput = null;
let confirmationInput = null;
let counter = 0;
let submitButton = null;
let preventAnimation = 0;

class CreateAccount extends Component {
    constructor() {
        super()
        this.state = {
            firstName: '',
            lastName: '',
            userName: '',
            password: '',
            passwordConfirmation: '',
            firstNameErrorMessage: '',
            lastNameErrorMessage: '',
            userNameErrorMessage: '',
            passwordErrorMessage: '',
            passwordConfirmationErrorMessage: '',
            responseMessage: ''
        };
        this.emailVerification = this.emailVerification.bind(this);
        this.handleFirstnameChange = this.handleFirstnameChange.bind(this);
        this.handleLastnameChange = this.handleLastnameChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleConfirmationChange = this.handleConfirmationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.passwordVerification = this.passwordVerification.bind(this);
        this.preventAccountCreation = this.preventAccountCreation.bind(this);
        this.updateErrorMessages = this.updateErrorMessages.bind(this);

    }

    handleFirstnameChange(event) {
        event.preventDefault();
        this.setState({ firstName: event.target.value });
        this.updateErrorMessages();
    }

    handleLastnameChange(event) {
        event.preventDefault();
        this.setState({ lastName: event.target.value });
        this.updateErrorMessages();
    }

    handleUsernameChange(event) {
        event.preventDefault();
        this.setState({ userName: event.target.value });
        this.updateErrorMessages();
    }

    handlePasswordChange(event) {
        event.preventDefault();
        this.setState({ password: event.target.value });
        this.updateErrorMessages();
    }

    handleConfirmationChange(event) {
        event.preventDefault();
        this.setState({ passwordConfirmation: event.target.value });
        this.updateErrorMessages();
    }

    handleSubmit(event) {
        event.preventDefault();
        this.emailVerification();
        this.passwordVerification();
        if (this.preventAccountCreation() === false) {
            return;
        }
        else {
            fetch('/signUp', {
                method: 'POST',
                body: (JSON.stringify({
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    username: this.state.userName,
                    password: this.state.password
                }))
            })
                .then(x => x.text())
                .then(res => {
                    let body = JSON.parse(res);
                    if (body.message === "success"){
                        this.props.history.push("/");
                    }
                })


            if (this.state.responseMessage === "failure. email does not exist") {
                console.log(this.state.responseMessage);
                this.setState({ userNameErrorMessage: 'Given email does not exists. Try again' });
                document.getElementById('email_account').style.borderColor = 'red';
                this.preventAccountCreation();
            }
        }
    }

    preventAccountCreation() {
        /*
            If the entered password and the confirmation password are the same, AND all the fields were filled, 
            user can therefore submit form
        */

        if (this.state.userNameErrorMessage === 'Given email does not exists. Try again') {
            canSubmitAccountCreationForm = false;
            document.getElementById("email_account").value = "";     //empty input textbox
        }
        if (this.state.firstName.length === 0) {
            this.setState({ firstNameErrorMessage: "First Name Required. Please enter your first name" });
            canSubmitAccountCreationForm = false;
            document.getElementById('firstname_account').style.borderColor = 'red';
        }
        if (this.state.lastName.length === 0) {
            this.setState({ lastNameErrorMessage: "Last Name Required. Please enter your last name" });
            canSubmitAccountCreationForm = false;
            document.getElementById('lastname_account').style.borderColor = 'red';
        }
        if (this.state.userName.length === 0) {
            this.setState({ userNameErrorMessage: "Email Required. Please enter your email address" })
            canSubmitAccountCreationForm = false;
            document.getElementById('email_account').style.borderColor = 'red';
        }
        if (this.state.password.length === 0) {
            this.setState({
                passwordErrorMessage: `Password Required 
                                    Please enter a password`})
            canSubmitAccountCreationForm = false;
            document.getElementById('password_account').style.borderColor = 'red';
        }
        if (this.state.passwordConfirmation.length === 0) {
            this.setState({ passwordConfirmationErrorMessage: "Password Confirmation Required. Please Rewrite the password" })
            canSubmitAccountCreationForm = false;
            document.getElementById('confirmation_account').style.borderColor = 'red';
            document.getElementById('confirmation_account').value = "";      //empty input textbox
        }

        else if (this.state.password !== this.state.passwordConfirmation) {
            preventAnimation++;
            this.setState({ passwordConfirmationErrorMessage: "Given passwords are not the same. Try again" });
            document.getElementById('confirmation_account').value = "";      //empty input textbox
        }
        return canSubmitAccountCreationForm;
    }


    emailVerification() {
        /*
            Verify if the given input by the userr contains @; generate an error message if not
        */
        if (this.state.userName.indexOf('@') === -1) {
            this.setState({ userNameErrorMessage: "Given input is not an email address. Try again" });
            document.getElementById('email_account').style.borderColor = 'red';
            document.getElementById('email_account').value = "";         //empty the input textbox

        }
    }

    passwordVerification() {
        /*
            Verify whether the given password contains at least 6 characters; generate an error message if not
        */
        if (this.state.password.length < 6) {
            this.setState({ passwordErrorMessage: "Password too short. Try again" });
            document.getElementById('password_account').style.borderColor = 'red';
            document.getElementById('password_account').value = "";    //empty the  input textbox
        }
    }




    updateErrorMessages() {

        if (this.state.firstName.length > 0) {
            this.setState({ firstNameErrorMessage: '' });

        }

        if (this.state.lastName.length > 0) {
            this.setState({ lastNameErrorMessage: '' });

        }

        if (this.state.userName.length > 0) {
            this.setState({ userNameErrorMessage: '' });
        }

        if (this.state.password.length > 0) {
            this.setState({ passwordErrorMessage: '' });

        }

        if (this.state.passwordConfirmation.length > 0) {
            this.setState({ passwordConfirmationErrorMessage: '' });
        }
    }


    render() {
        return (
            <div className="createAccountPage comp">
                <div id="headerCA">
                    <ul>
                        <li>
                            <div>{"FREE Shipping on all orders across Canada!"}</div>
                        </li>
                        <li>{"ALIBAY"}</li>
                        <li></li>
                    </ul>
                </div>

                <div id="accountForm">
                    <p id="title_account">{"Create Account"}</p>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" onChange={this.handleFirstnameChange} placeholder="First Name" id="firstname_account" />
                        <p id="firstNameError">{this.state.firstNameErrorMessage}</p>
                        <input type="text" onChange={this.handleLastnameChange} placeholder="Last Name" id="lastname_account" />
                        <p id="lastNameError">{this.state.lastNameErrorMessage}</p>
                        <input type="text" onChange={this.handleUsernameChange} placeholder="Email Address" id="email_account" />
                        <p id="userNameError">{this.state.userNameErrorMessage}</p>
                        <input type="text" onChange={this.handlePasswordChange} placeholder="Password (min 6 Characters)" id="password_account" />
                        <p id="passwordError">{this.state.passwordErrorMessage}</p>
                        <input type="text" onChange={this.handleConfirmationChange} placeholder="Confirm Password" id="confirmation_account" />
                        <p id="passwordConfirmationError">{this.state.passwordConfirmationErrorMessage}</p>
                        <input type="submit" onClick={this.handleSubmit} value="REGISTER" id="signUp_account" />
                    </form>
                </div>

                <div className="footer">
                    <div className="footerSlogan">
                        <p>{"ALIBAY"}</p>
                        <p>{"ONCE YOU TRY IT, YOU ADOPT IT"}</p>
                        <ul>
                            <li>{"Email Address: theRealAlibay@gmail.com"}</li>
                            <li>{"Phone Number: 1(1800)-BELIEVE"}</li>
                            <li>{"Copyright 2018"}</li>
                        </ul>
                    </div>
                </div>                

                {
                    document.addEventListener("DOMContentLoaded",

                        function () {
                            counter++;
                            document.getElementById("firstname_account").addEventListener('focus',
                                function () {
                                    firstNameInput = anime({
                                        targets: document.getElementById("firstname_account"),
                                        borderColor: '#ffc3bc'
                                    })
                                })

                            document.getElementById("lastname_account").addEventListener('focus',
                                function () {
                                    counter++;
                                    lastNameInput = anime({
                                        targets: document.getElementById("lastname_account"),
                                        borderColor: '#ffc3bc'
                                    })
                                })
                            document.getElementById("email_account").addEventListener('focus',
                                function () {
                                    counter++;
                                    emailInput = anime({
                                        targets: document.getElementById("email_account"),
                                        borderColor: '#ffc3bc'
                                    })
                                })

                            document.getElementById("password_account").addEventListener('focus',
                                function () {
                                    counter++
                                    passwordInput = anime({
                                        targets: document.getElementById("password_account"),
                                        borderColor: '#ffc3bc'
                                    })
                                })

                            document.getElementById("confirmation_account").addEventListener('focus',
                                function () {
                                    counter++
                                    confirmationInput = anime({
                                        targets: document.getElementById("confirmation_account"),
                                        borderColor: '#ffc3bc'
                                    })
                                })

                            document.getElementById('confirmation_account').addEventListener('focus',
                                function () {
                                    counter++;
                                    if (preventAnimation > 0) {
                                        return;
                                    }
                                    if (counter > 5) {
                                        submitButton = anime({
                                            targets: document.getElementById('signUp_account'),
                                            borderColor: '#39ff14',
                                            borderWidth: 6,
                                            color: '#2aaf11',
                                            width: 150,
                                            height: 45,
                                            left: 440,
                                            delay: 2000,
                                            duration: 15000,
                                            borderRadius: 15
                                        })
                                    }
                                }
                            )
                        }
                    )
                }
            </div>
        );
    }
}

export default withRouter(CreateAccount);