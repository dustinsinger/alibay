import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css'

class ItemsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userID: this.props.userID,
            heading: "",
            items: []
        }
    }

    componentDidMount() {
        if (this.props.type === "sold") {
            this.setState({ heading: "Items Sold" })
            fetch('/itemsSold', {
                method: 'POST',
                body: JSON.stringify({ userID: this.state.userID })
            })
                .then(x => x.text())
                .then(response => {
                    let itemsSold = JSON.parse(response)
                    this.setState({items: itemsSold})
                })
        }
        else if (this.props.type === "bought") {
            this.setState({ heading: "Items Bought" })
            fetch('/itemsBought', {
                method: 'POST',
                body: JSON.stringify({ userID: this.state.userID })
            })
                .then(x => x.text())
                .then(response => {
                    let itemsBought = JSON.parse(response)
                    this.setState({items: itemsBought})
                })
        }
    }

    render() {
        return (
            <div className="App">
                <h2>{this.state.heading}</h2>
                <div>{this.state.items.map((x, i) => <div key={i}>{x.name}, {x.price}, {x.description}</div>)}</div>
            </div>
        );
    }
}

export default ItemsList;